from drb.drivers.file import DrbFileNode
from drb.drivers.json import JsonNodeFactory, JsonBaseNode
import io


array_path = 'tests/resources/array.json'

node = JsonNodeFactory().create(array_path)

DICT = {
    "value1": "toot",
    "value2": "2",
    "value3": [3, 2]
}

node = JsonNodeFactory().create(DICT)

file_node = DrbFileNode(array_path)
with io.FileIO(array_path) as stream:
    JsonBaseNode(file_node, stream)
