from drb.drivers.json import JsonNode

geo_path = 'tests/resources/geo.json'

node = JsonNode(geo_path)

# print FeatureCollection
print(node['type'].value)

# print Point
print(node['features']['geometry']['type'].value)

# print [102.0, 0.0]
print(node['features', 1]['geometry']['coordinates'].value)

# print [105.0, 1.0]
print(node['features', 1]['geometry']['coordinates', 3].value)

""" print 
{
    'type': 'Feature', 
    'geometry': {
        'type': 'Point', 
        'coordinates': [102.0, 0.5]
    }, 
    'properties': {
        'prop0': 'value0'
    }
}
"""
print(node['features'].value)
