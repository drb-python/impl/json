.. _example:

Example
=======

Load a json file or a Dict
---------------------------
.. literalinclude:: example/load.py
    :language: python

Navigate in a JsonNode
-----------------------
.. literalinclude:: example/navigate.py
    :language: python
