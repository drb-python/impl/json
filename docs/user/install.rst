.. _install:

Installation of json implementation
====================================
Installing ``drb-driver-json`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-json
